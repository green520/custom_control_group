package king.com.customprogressbar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import king.com.customprogressbar.activity.MainActivity;
import king.com.customprogressbar.activity.PieChartActivity;
import king.com.customprogressbar.activity.TranserProgressActivity;
import king.com.customprogressbar.activity.YbeActivity;

/**
 * Created by Administrator on 2018/5/25.
 */

public class ListActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_layout);

        findViewById(R.id.one).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListActivity.this, MainActivity.class));
            }
        });
        findViewById(R.id.two).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListActivity.this, PieChartActivity.class));
            }
        });
        findViewById(R.id.there).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListActivity.this, YbeActivity.class));
            }
        });
        findViewById(R.id.four).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListActivity.this, TranserProgressActivity.class));
            }
        });
    }
}
