package king.com.customprogressbar.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import king.com.customprogressbar.R;
import king.com.customprogressbar.bean.PieData;
import king.com.customprogressbar.view.PieChartView;

/**
 * Created by Administrator on 2018/5/25.
 * 饼状图
 */

public class PieChartActivity extends Activity {
    PieChartView piech_id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.piechart_layout);

        piech_id = findViewById(R.id.piech_id);


        ArrayList<PieData> datas = new ArrayList<>();
        PieData pieData = new PieData("sloop1", 80);
        PieData pieData2 = new PieData("sloop2", 40);
        PieData pieData3 = new PieData("sloop3", 60);
        datas.add(pieData);
        datas.add(pieData2);
        datas.add(pieData3);
        piech_id.setData(datas);
    }
}
