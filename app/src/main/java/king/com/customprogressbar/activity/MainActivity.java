package king.com.customprogressbar.activity;

import android.app.Activity;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import king.com.customprogressbar.R;
import king.com.customprogressbar.bean.PieData;
import king.com.customprogressbar.view.CircularProgressBar;
import king.com.customprogressbar.view.PieChartView;

public class MainActivity extends Activity {

    CircularProgressBar roundProgressBar2;
    Handler handler = new Handler();;
    int time =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

    }

    private void initView() {
        roundProgressBar2 = findViewById(R.id.am_progressbar_one);

//        roundProgressBar2.setInsideColor(R.color.inside_color);//设置为完成进度的颜色
        roundProgressBar2.setOutsideColor(R.color.colorAccent);//设置进度的颜色
       new Thread(new MyRunnable()).start();
    }

    class MyRunnable implements Runnable{
        @Override
        public void run() {
            while (time <101){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        roundProgressBar2.setProgress(time);
                    }
                });
                try {
                    Thread.sleep(1000);//线程休眠1秒再继续执行
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                time++;
            }
        }
    }
}
