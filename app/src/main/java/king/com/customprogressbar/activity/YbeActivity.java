package king.com.customprogressbar.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.ArrayList;

import king.com.customprogressbar.R;
import king.com.customprogressbar.bean.Panel;
import king.com.customprogressbar.bean.PieData;
import king.com.customprogressbar.view.PanelPieChartLabel;

/**
 * Created by Administrator on 2018/5/25.
 */

public class YbeActivity extends Activity {

    PanelPieChartLabel panel_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
           setContentView(R.layout.panel_layout);
        panel_id = findViewById(R.id.panel_id);
        /*PanelPieChartLabel mPanelPieChartLabel = new PanelPieChartLabel(this);
        setContentView(mPanelPieChartLabel);*/
        // 颜色表
         int[] mColors = {0xFFCCFF00, 0xFF6495ED, 0xFFE32636, 0xFF800000, 0xFF808000, 0xFFFF8C69, 0xFF808080,
                0xFFE6B800, 0xFF7CFC00};
        ArrayList<Panel> datas = new ArrayList<>();
        //(String name, float value, float percentage, int color, float angle)
        Panel pieData = new Panel("sloop1", 80,0xFF800000);
        Panel pieData2 = new Panel("sloop2", 40,0xFF6495ED);
        Panel pieData3 = new Panel("sloop3", 110,0xFFFF8C69);
        Panel pieData4 = new Panel("sloop4", 60,0xFF808080);
        Panel pieData5 = new Panel("sloop5", 30,0xFF7CFC00);
        Panel pieData6 = new Panel("sloop6", 30,0xFFCCFF00);
        Panel pieData7 = new Panel("sloop7", 30,0xFF6495ED);
        datas.add(pieData);
        datas.add(pieData2);
        datas.add(pieData3);
        datas.add(pieData4);
        datas.add(pieData5);
        datas.add(pieData6);
        datas.add(pieData7);
        panel_id.setData(datas);
    }






    /*class PanelPieChartLabel extends View {
        private int ScrWidth,ScrHeight;
        public PanelPieChartLabel(Context context) {
            super(context);
            // TODO Auto-generated constructor stub

            //解决4.1版本 以下canvas.drawTextOnPath()不显示问题
            this.setLayerType(View.LAYER_TYPE_SOFTWARE,null);

            //屏幕信息
            DisplayMetrics dm = getResources().getDisplayMetrics();
            ScrHeight = dm.heightPixels;
            ScrWidth = dm.widthPixels;
        }

        public void onDraw(Canvas canvas){
            //画布背景
            canvas.drawColor(Color.WHITE);

            //画笔初始化
            Paint PaintArc = new Paint();
            PaintArc.setColor(Color.RED);

            Paint PaintGree = new Paint();
            PaintGree.setColor(Color.GREEN);
            PaintGree.setStyle(Paint.Style.FILL);

            Paint PaintBlue = new Paint();
            PaintBlue.setColor(Color.BLUE);
            PaintBlue.setStyle(Paint.Style.FILL);

            Paint PaintYellow = new Paint();
            PaintYellow.setColor(Color.YELLOW);
            PaintYellow.setStyle(Paint.Style.FILL);

            //抗锯齿
            PaintArc.setAntiAlias(true);
            PaintYellow.setAntiAlias(true);
            PaintGree.setAntiAlias(true);

            PaintBlue.setTextSize(20);

            float cirX = ScrWidth / 2;
            float cirY = ScrHeight / 3 ;
            float radius = ScrHeight / 5 ;
            //先画个圆确定下显示位置
            canvas.drawCircle(cirX,cirY,radius,PaintGree);

            float arcLeft = cirX - radius;
            float arcTop  = cirY - radius ;
            float arcRight = cirX + radius ;
            float arcBottom = cirY + radius ;
            RectF arcRF0 = new RectF(arcLeft ,arcTop,arcRight,arcBottom);

            *//*-------------------------------------------------------------------------------------*//*
            //饼图标题
            //canvas.drawText("author:xiongchuanliang",60,70 , PaintBlue);

            //位置计算类
            XChartCalc xcalc = new XChartCalc();

            //实际用于计算的半径
            float calcRadius = radius/2;
            *//*-------------------------------------------------------------------------------------*//*
            //初始角度
            float pAngle1 = 130f;
            float pAngle2 = 40f;
            float pAngle3 = 360f - pAngle1 - pAngle2;

            //填充扇形
            canvas.drawArc(arcRF0, 0,pAngle1, true,PaintArc);

            //计算并在扇形中心标注上百分比    130%
            xcalc.CalcArcEndPointXY(cirX, cirY, calcRadius, pAngle1/2);
            canvas.drawText(Float.toString(pAngle1)+"%", xcalc.getPosX(),xcalc.getPosY(), PaintBlue);
             *//*-------------------------------------------------------------------------------------*//*

             *//*-------------------------------------------------------------------------------------*//*
            //填充扇形
            canvas.drawArc(arcRF0, pAngle1,pAngle2, true,PaintYellow);
            //计算并在扇形中心标注上百分比   40%
            xcalc.CalcArcEndPointXY(cirX, cirY, calcRadius, pAngle1 + pAngle2/2);
            canvas.drawText(Float.toString(pAngle2)+"%", xcalc.getPosX(),xcalc.getPosY(), PaintBlue);

            *//*-------------------------------------------------------------------------------------*//*
            //计算并在扇形中心标注上百分比  190%
            xcalc.CalcArcEndPointXY(cirX, cirY, calcRadius, pAngle1 + pAngle2 + pAngle3/2);
            canvas.drawText(Float.toString(pAngle3)+"%", xcalc.getPosX(),xcalc.getPosY(), PaintBlue);
             *//*-------------------------------------------------------------------------------------*//*
        }


    }*/
}
