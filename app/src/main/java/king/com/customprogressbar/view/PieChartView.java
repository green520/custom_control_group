package king.com.customprogressbar.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import king.com.customprogressbar.R;
import king.com.customprogressbar.activity.XChartCalc;
import king.com.customprogressbar.bean.PieData;

/**
 * Created by Administrator on 2018/5/25.
 * 饼状图
 */

public class PieChartView extends View {

    // 颜色表
    private int[] mColors = {0xFFCCFF00, 0xFF6495ED, 0xFFE32636, 0xFF800000, 0xFF808000, 0xFFFF8C69, 0xFF808080,
            0xFFE6B800, 0xFF7CFC00};
    // 饼状图初始绘制角度w
    private float mStartAngle = 0;
    // 数据
    private ArrayList<PieData> mData;
    // 宽高
    private int mWidth, mHeight;
    // 画笔
    private Paint mPaint = new Paint();
    private Paint mPaint1 = new Paint();
    //位置计算类
    XChartCalc xcalc = new XChartCalc();

    public PieChartView(Context context) {
        super(context,null);
    }

    public PieChartView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs,0);
        mPaint.setStyle(Paint.Style.FILL);//设置画笔填充模式
        mPaint.setAntiAlias(true);
         mPaint1.setStyle(Paint.Style.FILL);//设置画笔填充模式
        mPaint1.setAntiAlias(true);//
    }

    public PieChartView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.mWidth = w;
        this.mHeight = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (null == mData)
            return;
        float currentStartAngle = mStartAngle;                      // 当前起始角度
        canvas.translate(mWidth / 2, mHeight / 2);       // 将画布坐标原点移动到中心位置
//        float r = (float) Math.min(mWidth, mHeight) / 2;        // 饼状图半径
        float r = (float) Math.min(mWidth, mHeight) / 2;        // 饼状图半径
        RectF rect = new RectF(-r, -r, r, r);                     // 饼状图绘制区域
        for (int i = 0; i < mData.size(); i++) {
            PieData pie = mData.get(i);
            mPaint.setColor(pie.getColor());
            mPaint1.setColor(Color.BLUE);
            mPaint1.setTextSize(20);
            canvas.drawArc(rect, currentStartAngle, pie.getAngle(), true, mPaint);
            Log.e("lis",pie.getAngle()+"");
            //计算并在扇形中心标注上百分比
            xcalc.CalcArcEndPointXY(mWidth / 2, mHeight / 2,r/2,currentStartAngle+pie.getAngle()/2);
            canvas.drawText(pie.getName()+"",xcalc.getPosX()+0.0f,xcalc.getPosY()+0.0f,mPaint1);
            currentStartAngle += pie.getAngle();

//            canvas.save();
//            canvas.translate(-mWidth / 2, -mHeight / 2);
//            canvas.restore();
        }

    }

    // 设置起始角度
    public void setStartAngle(int mStartAngle) {
        this.mStartAngle = mStartAngle;
        invalidate();   // 刷新
    }

    // 设置数据
    public void setData(ArrayList<PieData> mData) {
        this.mData = mData;
        initData(mData);
        invalidate();   // 刷新
    }

    // 初始化数据
    private void initData(ArrayList<PieData> mData) {
        if (null == mData || mData.size() == 0)   // 数据有问题 直接返回
            return;

        float sumValue = 0;
        for (int i = 0; i < mData.size(); i++) {
            PieData pie = mData.get(i);
            sumValue += pie.getValue();       //计算数值和
            int j = i % mColors.length;       //设置颜色
            pie.setColor(mColors[j]);
        }
        float sumAngle = 0;
        for (int i = 0; i < mData.size(); i++) {
            PieData pie = mData.get(i);
            float percentage = pie.getValue() / sumValue;   // 百分比
            float angle = percentage * 360;                 // 对应的角度
            pie.setPercentage(percentage);                  // 记录百分比
            pie.setAngle(angle);                            // 记录角度大小
            sumAngle += angle;
            Log.e("angle", "" + pie.getAngle());
        }
    }

}
