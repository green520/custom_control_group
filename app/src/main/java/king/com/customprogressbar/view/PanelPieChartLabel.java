package king.com.customprogressbar.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import king.com.customprogressbar.activity.XChartCalc;
import king.com.customprogressbar.bean.Panel;

/**
 * Created by Administrator on 2018/5/25.
 */

public class PanelPieChartLabel extends View {

    private ArrayList<Panel> mData;
    private float starAnger=0;//起始角度

    private int ScrWidth,ScrHeight;

    public PanelPieChartLabel(Context context) {
        super(context,null);
    }

    public PanelPieChartLabel(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs,0);
    }

    public PanelPieChartLabel(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int size = MeasureSpec.getSize(widthMeasureSpec);
        int mode = MeasureSpec.getMode(widthMeasureSpec);

        if (mode == MeasureSpec.EXACTLY) {
            ScrWidth = size;
        } else {
            //ScrWidth = (int) ((2 * outsideRadius) + progressWidth);
            ScrWidth = size;
        }
        size = MeasureSpec.getSize(heightMeasureSpec);
        mode = MeasureSpec.getMode(heightMeasureSpec);
        if (mode == MeasureSpec.EXACTLY) {
            ScrHeight = size;
        } else {
            //ScrHeight = (int) ((2 * outsideRadius) + progressWidth);
            ScrHeight = size;
        }
        setMeasuredDimension(ScrWidth, ScrHeight);
    }

    public void onDraw(Canvas canvas){
        //画布背景
        canvas.drawColor(Color.argb(0,0,0,0));

        //画笔初始化
        Paint PaintArc1 = new Paint();
        PaintArc1.setStyle(Paint.Style.FILL);;

        Paint PaintGree = new Paint();
        PaintGree.setARGB(0,0,0,0);
        PaintGree.setStyle(Paint.Style.FILL);

        Paint PaintBlue = new Paint();
        PaintBlue.setColor(Color.BLUE);
        PaintBlue.setStyle(Paint.Style.FILL);

        //抗锯齿
        PaintArc1.setAntiAlias(true);
        PaintGree.setAntiAlias(true);

        PaintBlue.setTextSize(20);

        float cirX = ScrWidth / 2;
        float cirY = ScrHeight / 2 ;
        float radius = ScrHeight / 2 ;
        //先画个圆确定下显示位置
        canvas.drawCircle(cirX,cirY,radius,PaintGree);

        float arcLeft = cirX - radius;
        float arcTop  = cirY - radius ;
        float arcRight = cirX + radius ;
        float arcBottom = cirY + radius ;
        RectF arcRF0 = new RectF(arcLeft ,arcTop,arcRight,arcBottom);

        //饼图标题
        //canvas.drawText("author:xiongchuanliang",60,70 , PaintBlue);
        //位置计算类
        XChartCalc xcalc = new XChartCalc();
        //实际用于计算的半径
        float calcRadius = radius/2;

        for(int i=0;i<mData.size();i++){
            Panel panel = mData.get(i);
            //填充扇形
            Log.e("Color",panel.getColor()+"");
            PaintArc1.setColor(panel.getColor());
            canvas.drawArc(arcRF0, starAnger,panel.getAngle(), true,PaintArc1);

            //计算并在扇形中心标注上对应的信息
            xcalc.CalcArcEndPointXY(cirX, cirY, calcRadius*1.6f,starAnger+panel.getAngle()/2);
            canvas.drawText(panel.getName(), xcalc.getPosX(),xcalc.getPosY(), PaintBlue);
            starAnger += panel.getAngle();
        }
    }

    // 设置数据
    public void setData(ArrayList<Panel> mData) {
        this.mData = mData;
        initData(mData);
        invalidate();   // 刷新
    }

    // 初始化数据
    private void initData(ArrayList<Panel> mData) {
        if (null == mData || mData.size() == 0)   // 数据有问题 直接返回
            return;

        float sumValue = 0;
        for (int i = 0; i < mData.size(); i++) {
            Panel pie = mData.get(i);
            sumValue += pie.getValue();       //计算数值和
        }
        float sumAngle = 0;
        for (int i = 0; i < mData.size(); i++) {
            Panel pie = mData.get(i);
            float percentage = pie.getValue() / sumValue;   // 百分比
            float angle = percentage * 360;                 // 对应的角度
            pie.setPercentage(percentage);                  // 记录百分比
            pie.setAngle(angle);                            // 记录角度大小
            sumAngle += angle;
            Log.e("angle", "" + pie.getAngle());
        }
    }
}
