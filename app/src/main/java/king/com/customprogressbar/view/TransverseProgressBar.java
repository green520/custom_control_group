package king.com.customprogressbar.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import king.com.customprogressbar.R;

/**
 * Created by Administrator on 2018/5/25.
 * 自定义带有状态的进度条
 */

public class TransverseProgressBar extends View {

    private Paint mPaint;//圆点画笔
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private int mWidth,mHeight;//宽高
    private int allNumber;//所有的状态总数
    private int nowNum;//当前节点数下标0开始
    private int nodeRadius;//圆点半径

    private int currNodeState;//当前节点是否成功

    private Drawable progresSuccDrawable;//成功节点
    private Drawable progresFailDrawable;//失败节点
    private Drawable progressingDrawable;//
    private Drawable unprogressingDrawable;//

    private ArrayList<Node> nodes;
    private List<String> text = new ArrayList<>();

    public TransverseProgressBar(Context context) {
        super(context,null);
    }

    public TransverseProgressBar(Context context,  AttributeSet attrs) {
        super(context, attrs,0);
        TypedArray mTypedArray = context.obtainStyledAttributes(attrs,R.styleable.TransverseProgressBar);
        progressingDrawable = mTypedArray.getDrawable(R.styleable.TransverseProgressBar_progressingDrawable);
        unprogressingDrawable = mTypedArray.getDrawable(R.styleable.TransverseProgressBar_unprogressingDrawable);
        progresFailDrawable = mTypedArray.getDrawable(R.styleable.TransverseProgressBar_progresFailDrawable);
        progresSuccDrawable = mTypedArray.getDrawable(R.styleable.TransverseProgressBar_progresSuccDrawable);
    }

    public TransverseProgressBar(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mWidth = getMeasuredWidth();// MeasureSpec.getSize(widthMeasureSpec);
        mHeight = getMeasuredHeight();// MeasureSpec.getSize(heightMeasureSpec);


        mBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
        mPaint = new Paint();
        mPaint.setColor(Color.GREEN);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeJoin(Paint.Join.ROUND); // 圆角
        mPaint.setStrokeCap(Paint.Cap.ROUND); // 圆角
        mCanvas = new Canvas(mBitmap);
        nodes = new ArrayList<TransverseProgressBar.Node>();
        float nodeWidth = ((float)mWidth)/(allNumber-1);
        Log.e("listew",nodeWidth+"---"+mHeight+"-="+nodeRadius+"==="+allNumber);
        for(int i=0;i<allNumber;i++){
            Node node = new Node();
            if(i==0)
                node.point = new Point( (int) nodeWidth*i,mHeight/2-nodeRadius);
            else if(i==(allNumber-1))
                node.point = new Point(((int)nodeWidth*i)-nodeRadius*2,mHeight/2-nodeRadius);
            else
                node.point = new Point(((int)nodeWidth*i)-nodeRadius,mHeight/2-nodeRadius);

            if(nowNum==i)
                node.type=1;//未完成
            else
                node.type=0;//完成
            nodes.add(node);
        }
    }

    public  void setDate(int allNumber,int nowNum,int currNodeState,int nodeRadius,ArrayList<String> text){
        this.allNumber = allNumber;
        this.nowNum = nowNum;
        this.currNodeState = currNodeState;
        this.nodeRadius = nodeRadius;
        this.text = text;
        Log.e("dddd",allNumber+"--"+nowNum+"--"+currNodeState+"===="+text.toString());
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        DrawProgerss();
        if(mBitmap!=null){
            canvas.drawBitmap(mBitmap, new Rect(0,0,mBitmap.getWidth(),mBitmap.getHeight()), new Rect(0,0,mBitmap.getWidth(),mBitmap.getHeight()), mPaint);
        }
        Log.e("size",nodes.size()+"---------"+text.size());
       for(int i=0;i<nodes.size();i++){
           Node node = nodes.get(i);
           Log.v("ondraw", node.point.x +";y="+node.point.y);
           if(i<nowNum)  //已完成的进度节点
           {
               Log.e("point",node.point.x+"=="+node.point.y+"=="+(node.point.x + nodeRadius*2)+"=="+(node.point.y + nodeRadius*2));
               progressingDrawable.setBounds(node.point.x,  node.point.y , node.point.x + nodeRadius*2,node.point.y + nodeRadius*2);
               progressingDrawable.draw(canvas);
           }
           else if(i==nowNum)  //当前所到达的进度节点（终点）
           {
               if(currNodeState == 1) //判断是成功还是失败  0 :失败  1：成功
               {
                   progresSuccDrawable.setBounds(node.point.x,  node.point.y , node.point.x + nodeRadius*2,node.point.y + nodeRadius*2);
                   progresSuccDrawable.draw(canvas);
               }
               else
               {
                   progresFailDrawable.setBounds(node.point.x,  node.point.y , node.point.x + nodeRadius*2,node.point.y + nodeRadius*2);
                   progresFailDrawable.draw(canvas);
               }
           }
           else   //未完成的进度节点
           {
               unprogressingDrawable.setBounds(node.point.x,  node.point.y , node.point.x + nodeRadius*2,node.point.y + nodeRadius*2);
               unprogressingDrawable.draw(canvas);
           }
       }
       Log.e("textsize",text.size()+"--");
        mPaint.setColor(Color.BLUE);
        mPaint.setTextSize(18);
       for(int j=0;j<text.size();j++){
           String string = text.get(j);
           Log.e("textstringsize",string+"--");
           canvas.drawText(string,nodes.get(j).point.x + nodeRadius/2,nodes.get(j).point.y+ nodeRadius*3 ,mPaint);
       }
    }
    private void DrawProgerss(){
        //先画背景
        Paint bgPaint = new Paint();
//        bgPaint.setColor(Color.parseColor("#f0f0f0"));
        bgPaint.setColor(Color.argb(0,0,0,0));
        mCanvas.drawRect(0, 0, mWidth, mHeight, bgPaint);
        //先画线段，线段的高度为nodeRadius/2
        mPaint.setStrokeWidth(nodeRadius/2);
        //前半截线段
//		mCanvas.drawLine(nodeRadius, mHeight/2, mWidth-nodeRadius, mHeight/2, mPaint);  //线段2端去掉nodeRadius
        mCanvas.drawLine(nodeRadius, mHeight/2, nodes.get(nowNum).point.x + nodeRadius, nodes.get(nowNum).point.y + nodeRadius, mPaint);  //线段2端去掉nodeRadius
        //后半截线段
        mPaint.setColor(Color.parseColor("#dddddd"));
        mCanvas.drawLine(nodes.get(nowNum).point.x +nodeRadius, nodes.get(nowNum).point.y + nodeRadius, mWidth-nodeRadius, mHeight/2, mPaint);  //线段2端去掉nodeRadius
    }
    class Node{
        int type;
        Point point;
    }
}
